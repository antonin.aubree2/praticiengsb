package com.example.gestionpraticien;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;

public class PraticienActivity extends AppCompatActivity {
    public  TextView textViewDepartement;
    ListView listPraticiens;
    private static final String TAG_num = "PRA_NUM";
    private static final String TAG_nom = "PRA_NOM";
    private static final String TAG_prenom = "PRA_PRENOM";
    private static final String TAG_adresse = "Pra_ADRESSE";
    private static final String TAG_ville = "PRA_VILLE";
    private static final String TAG_cp = "PRA_CP";
    private String CpSelected;
    ArrayList<Praticien> oslist = new ArrayList<Praticien>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_praticien);
        textViewDepartement = (TextView) findViewById((R.id.textViewDepartement)) ;
        CpSelected = this.getIntent().getExtras().getString("CP");
        textViewDepartement.setText("Département sélectionné : " + CpSelected);
        if((CpSelected.substring(0,1)) == "0")
        {
            CpSelected = CpSelected.substring(1,2) + "000";
        }
        else
        {
            CpSelected = CpSelected + "000";
        }
        new JSONAsynchrone().execute();
    } // fin de onCreate
    private class JSONAsynchrone extends AsyncTask<String, String, String> {
        private ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... strings) {
            String newUrl = "http://192.168.1.20/android/TousPracticiensGsbJson.php?cp=" + CpSelected;
            Log.i("e",newUrl);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(newUrl).build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PraticienActivity.this);
            pDialog.setMessage("Je charge les données ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected void onPostExecute(String json) {
            pDialog.dismiss();
            //  textViewDonnees.setText(json.toString());
            JSONObject c=null;
            try {
                // Getting JSON Array from URL
                JSONArray android = new JSONArray(json);
                for (int i = 0; i < android.length(); i++) {
                    c = android.getJSONObject(i);
                    int id = c.getInt(TAG_num);
                    String nom = c.getString(TAG_nom);
                    String prenom = c.getString(TAG_prenom);
                    String adresse = c.getString(TAG_adresse);
                    String ville = c.getString(TAG_ville);
                    String cp = c.getString(TAG_cp);
                    Praticien vis = new Praticien(id, nom, prenom, adresse, ville, cp);
                    oslist.add(vis);
                }
                listPraticiens = (ListView) findViewById(R.id.listViewPraticien);

                CustomAdapter monAdapter = new CustomAdapter( getBaseContext(), oslist);

                listPraticiens.setAdapter(monAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.i("e",e.toString());
            }
        }
    }
}
