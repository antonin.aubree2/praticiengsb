package com.example.gestionpraticien;

public class Dep {
    String numCp;

    public Dep(String cp)
    {
        numCp = cp;
    }

    public String getNumDep(){
        return numCp;
    }

    @Override
    public String toString() {
        return "dep : " + numCp;
    }
}
