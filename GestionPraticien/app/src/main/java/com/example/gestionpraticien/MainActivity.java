package com.example.gestionpraticien;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
    // le textView a été déclaré ici pour être valable dans toutes les méthodes
    public  TextView textViewDonnees;
    ListView listDepartements;
    private static final String TAG_CP = "PRA_CP";
    ArrayList<Dep> oslist = new ArrayList<Dep>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonCharger = (Button) findViewById(R.id.buttonCharger);
        textViewDonnees = (TextView) findViewById((R.id.textViewDonnees));


        buttonCharger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new JSONAsynchrone().execute();
            }
        });
    } // fin de onCreate
    private class JSONAsynchrone extends AsyncTask<String, String, String> {
        //mise en place d’une fenêtre de dialogue
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Je charge les données ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url("http://192.168.1.20/android/TousDepartementsGsbJson.php").build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            pDialog.dismiss();
            JSONObject c=null;
            try {
                // Getting JSON Array from URL
                Log.i("e",json);
                JSONArray android = new JSONArray(json);
                for(int i = 0; i < android.length(); i++){
                    c = android.getJSONObject(i);
                    String cp = c.getString(TAG_CP);
                    if (cp.length() == 5)
                    {
                        cp = cp.substring(0,2);
                    }
                    else
                    {
                        cp = cp.substring(0,1);

                        cp = "0" + cp;
                    }
                    Dep dep = new Dep(cp);
                    oslist.add(dep);

                }	// fin du for
                // textViewDonnees.setText(oslist.get(i).toString());
                listDepartements=(ListView)findViewById(R.id.listDepartements);
                final ArrayAdapter<Dep> adapter = new ArrayAdapter<Dep>(MainActivity.this,R.layout.support_simple_spinner_dropdown_item, oslist);
                listDepartements.setAdapter(adapter);

                listDepartements.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                        String CP = oslist.get(+position).getNumDep();
                        Intent intentPraticien = new Intent ( MainActivity.this, PraticienActivity.class);
                        intentPraticien.putExtra("CP", CP);
                        startActivity(intentPraticien);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
                Log.i("e", e.toString());
            }
        }
    }
}
