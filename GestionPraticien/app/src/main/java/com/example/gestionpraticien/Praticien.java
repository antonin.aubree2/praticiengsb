package com.example.gestionpraticien;

public class Praticien {
    int numPra;
    String nomPra;
    String prenomPra;
    String ruePra;
    String cpPra;
    String villePra;
    public Praticien(int num, String nom, String prenom, String rue, String cp, String ville)
    {
        numPra = num;
        nomPra = nom;
        prenomPra = prenom;
        ruePra = rue;
        cpPra = cp;
        villePra = ville;
    }
    public int getNumPra()
    {
        return numPra;
    }
    public String getNomPra()
    {
        return nomPra;
    }
    public String getPrenomPra()
    {
        return prenomPra;
    }
    public String getRuePra()
    {
        return ruePra;
    }
    public  String getCpPra()
    {
        return cpPra;
    }
    public String getVillePra()
    {
        return villePra;
    }
    @Override
    public String toString()
    {
        return numPra + " " + nomPra + " " + prenomPra + " " + ruePra + " " + cpPra + " " + villePra;
    }
}
